import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';

import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  const { getByLabelText, findByTestId, findByLabelText } = render(<OrderComponent />);
  const input = getByLabelText('number-input');
  const button = getByLabelText('submit-button');
  // Mock数据请求
  const response = { data: { status: '完成' } };
  axios.get.mockResolvedValue(response);
  // 触发事件
  fireEvent.change(input, { target: { value: 2 } });
  fireEvent.click(button);
  // 给出断言
  const show = await findByTestId('status');
  const value = await findByLabelText('number-input');
  expect(value).toHaveAttribute('value', '2');
  expect(show).toHaveTextContent('完成');
  // --end->
});
